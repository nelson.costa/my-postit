import React from 'react';
import PropTypes from 'prop-types';
import Draggable from 'react-draggable';
import Remarkable from 'remarkable';
import DOMPurify from 'dompurify';
import { getCurrentTime } from '../utils/helpers.js';

let md = new Remarkable('commonmark').use(function(md) {
  // little hack to not load images
  md.core.ruler.push('image_to_text', function(state) {
    for (let i = 0; i < state.tokens.length; i++) {
      let tok = state.tokens[i];
      if (tok.children) {
        for (let j = 0; j < tok.children.length; j++) {
          let child = tok.children[j];
          if (child.type === 'image') {
            child.type = 'text';
            child.content = child.src;
          }
        }
      }
    }
  });
});

export default class Postit extends React.Component {
  static propTypes = {
    classType: PropTypes.string.isRequired,
    postitId: PropTypes.number.isRequired,
    initialData: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      updatedAt: PropTypes.string,
      zIndex: PropTypes.number,
      x: PropTypes.number,
      y: PropTypes.number
    }),
    callbackAdd: PropTypes.func.isRequired,
    callbackRemove: PropTypes.func.isRequired,
    callbackGetZIndex: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    const { title, description, updatedAt, zIndex, x, y } = props.initialData;

    this.state = {
      title: title || '',
      description: description || '',
      updatedAt: updatedAt || '',
      edit: false,
      zIndex: zIndex || 0,
      x: x || 0,
      y: y || 0
    };

    this.toggleEdit = this.toggleEdit.bind(this);
    this.handleDescriptionUpdate = this.handleDescriptionUpdate.bind(this);
    this.handleTitleUpdate = this.handleTitleUpdate.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.onClickAdd = this.onClickAdd.bind(this);
    this.onClickRemove = this.onClickRemove.bind(this);
    this.saveToStorage = this.saveToStorage.bind(this);
    this.removeFromStorage = this.removeFromStorage.bind(this);
    this.handleDragStart = this.handleDragStart.bind(this);
    this.handleDragStop = this.handleDragStop.bind(this);
  }

  handleDragStart(event, data) {
    const zIndex = this.props.callbackGetZIndex();
    this.setState({
      zIndex: zIndex,
      updatedAt: getCurrentTime()
    });
  }

  handleDragStop(event, data) {
    this.setState({
      x: data.x,
      y: data.y,
      updatedAt: getCurrentTime()
    });
  }

  handleTitleUpdate(event) {
    this.setState({
      title: event.target.value,
      updatedAt: getCurrentTime()
    });
  }

  onClickAdd() {
    this.props.callbackAdd();
  }

  onClickRemove() {
    const id = this.props.postitId;

    this.removeFromStorage(id);
    this.props.callbackRemove(id);
  }

  handleDescriptionUpdate(event) {
    this.setState({
      description: event.target.value,
      updatedAt: getCurrentTime()
    });
  }

  toggleEdit() {
    this.setState({
      edit: !this.state.edit
    });
  }

  handleKeyPress(event) {
    if (event.key === 'Escape') {
      this.toggleEdit();
    }
  }

  removeFromStorage(id) {
    let appData = JSON.parse(localStorage.getItem('my-postit-data') || '[]');
    if (appData.length <= 1) {
      return;
    }
    appData = appData.filter(function(el) {
      return el.id !== id;
    });
    localStorage.setItem('my-postit-data', JSON.stringify(appData));
  }

  saveToStorage() {
    let appData = JSON.parse(localStorage.getItem('my-postit-data') || '[]');
    // this will create a shallow copy!
    let postitData = Object.assign({}, this.state);
    // fill missing keys
    postitData.id = this.props.postitId;
    postitData.classType = this.props.classType;

    // remove old save
    appData = appData.filter(function(el) {
      return el.id !== postitData.id;
    });

    appData.push(postitData);
    localStorage.setItem('my-postit-data', JSON.stringify(appData));
  }

  componentDidUpdate() {
    this.saveToStorage();
    //componentDidUpdate() will not be invoked if shouldComponentUpdate() returns false.
  }

  render() {
    const { title, description, edit, updatedAt, zIndex, x, y } = this.state;
    const { classType } = this.props;
    const innerText = description
      ? DOMPurify.sanitize(md.render(description))
      : 'Description';

    return (
      <Draggable
        cancel="textarea"
        onStart={this.handleDragStart}
        onStop={this.handleDragStop}
        position={{ x, y }}
      >
        <div className={'postit '.concat(classType)} style={{ zIndex }}>
          <header className="postit__header">
            <button
              type="button"
              className="postit__button--add"
              onClick={this.onClickAdd}
            >
              &#43;
            </button>
            <button
              type="button"
              className="postit__button--remove"
              onClick={this.onClickRemove}
            >
              &times;
            </button>
          </header>
          <textarea
            className="postit__textarea postit--title"
            placeholder="Title"
            defaultValue={title}
            onChange={this.handleTitleUpdate}
            // onBlur={this.saveToStorage}
            rows={1}
            maxLength={27}
          />
          {edit ? (
            <textarea
              className="postit__textarea postit--description"
              value={description}
              placeholder="Description"
              onChange={this.handleDescriptionUpdate}
              onBlur={this.toggleEdit}
              onKeyDown={this.handleKeyPress}
              maxLength={200}
              autoFocus={true}
            />
          ) : (
            <div
              className="postit--description"
              onClick={this.toggleEdit}
              dangerouslySetInnerHTML={{
                __html: innerText
              }}
            />
          )}
          <footer className="postit__footer">{updatedAt}</footer>
        </div>
      </Draggable>
    );
  }
}
