import React from 'react';
import Postit from './Postit.jsx';

export default class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          x: 100,
          y: 100,
          id: 1,
          classType: 'postit--odd',
          title: 'Learn React',
          description:
            '#### TODO\r\n\r\n+ React lifecycle\r\n+ How to pass data\r\n  - props\r\n  - state\r\n    * use state correctly\r\n+ Handling Events\r\n+ Make postit app\n  - **Draggable !!!**'
        }
      ]
    };

    this.lastId = 1;
    this.last_zIndex = 0;

    this.newPostit = this.newPostit.bind(this);
    this.removePostit = this.removePostit.bind(this);
    this.getZIndex = this.getZIndex.bind(this);
  }

  getZIndex() {
    return ++this.last_zIndex;
  }

  newPostit() {
    this.lastId++;
    let classType = 'postit--odd';
    if (this.lastId % 2 === 0) {
      classType = 'postit--even';
    }

    this.setState({
      data: this.state.data.concat([{ id: this.lastId, classType: classType }])
    });
  }

  removePostit(id) {
    let data = this.state.data;
    if (data.length === 1) {
      return;
    }
    data = data.filter(function(el) {
      return el.id !== id;
    });
    this.setState({ data });
  }

  componentWillMount() {
    let data = JSON.parse(localStorage.getItem('my-postit-data') || '[]');

    if (data.length !== 0) {
      // update last key
      let lastId = 1;
      let last_zIndex = 0;
      data.forEach(function(element) {
        if (element.id > lastId) {
          lastId = element.id;
        }

        if (element.zIndex > last_zIndex) {
          last_zIndex = element.zIndex;
        }
      });

      this.lastId = lastId;
      this.last_zIndex = last_zIndex;
      this.setState({ data });
    }
  }

  render() {
    return (
      <div id="board">
        <h1>My Board:</h1>
        {this.state.data.map(item => (
          <Postit
            key={item.id}
            classType={item.classType}
            postitId={item.id}
            initialData={item}
            callbackAdd={this.newPostit}
            callbackRemove={this.removePostit}
            callbackGetZIndex={this.getZIndex}
          />
        ))}
      </div>
    );
  }
}
