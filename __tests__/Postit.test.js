import React from 'react';
import Postit from '../src/components/Postit.jsx';
import renderer from 'react-test-renderer';

const dumyFunc = function() {
  return false;
};

test('Postit description changes edit mode on click', () => {
  let props = {
    classType: 'postit--odd',
    initialData: {
      title: 'Test title',
      description: 'Test description',
      updatedAt: ''
    },
    postitId: 1,
    callbackAdd: dumyFunc,
    callbackRemove: dumyFunc,
    callbackGetZIndex: dumyFunc
  };

  const component = renderer.create(<Postit {...props} />);

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  // find description child
  let description = tree.children.find(function(element) {
    return element.props.className === 'postit--description';
  });

  // manually trigger click
  description.props.onClick();
  // re-render
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  description = tree.children.find(function(element) {
    return element.props.className === 'postit__textarea postit--description';
  });

  description.props.onKeyDown({ key: 'Escape' });
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
