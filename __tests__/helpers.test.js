import { getCurrentTime } from '../src/utils/helpers.js';

test('returns string', () => {
  expect(typeof getCurrentTime()).toBe('string');
});

test('returns a valid date', () => {
  // If the argument doesn't represent a valid date, NaN is returned.
  expect(isNaN(Date.parse(getCurrentTime()))).toBe(false);
});
